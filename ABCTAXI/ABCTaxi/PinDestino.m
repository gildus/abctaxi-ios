//
//  PinOrigen.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.

#import "PinDestino.h"


@implementation PinDestino


//creamos una clase custom de una anotacion en el mapa 
- (id) initWithTitle: (NSString *)newTitle location: (CLLocationCoordinate2D) location
{
    self =  [super init];
    
    if (self){
        _titulo = newTitle;
        _coordinate = location;
        
    }
    return self;
}
-(MKAnnotationView *) annotation
{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"PinDestino"];
    annotationView.enabled = YES;
    annotationView.canShowCallout = NO;
    annotationView.image = [UIImage imageNamed:@"marker_d.png"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    return annotationView;
}

@end
