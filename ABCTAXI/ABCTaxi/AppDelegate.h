//
//  AppDelegate.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic)NSString *id_not;
@property (strong,nonatomic)NSString *idemployee;
@property (strong,nonatomic)NSString *fullname;
@property (strong,nonatomic)NSString *phone;
@property (strong,nonatomic)NSString *photo;
@property (strong,nonatomic)NSString *dni;
@property (strong,nonatomic)NSString *licenseplate;
@property (strong,nonatomic)NSString *model;
@property (strong,nonatomic)NSString *color;
@property (strong,nonatomic)NSString *latitude;
@property (strong,nonatomic)NSString *longitude;
@property (strong,nonatomic)NSString *pushKey;

@end

