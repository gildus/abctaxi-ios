//
//  HistorialCellTableViewCell.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/16/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface HistorialCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *state_view;
@property (weak, nonatomic) IBOutlet UILabel *origen_label;
@property (weak, nonatomic) IBOutlet UILabel *destiny_label;
@property (weak, nonatomic) IBOutlet UILabel *conductor_label;
@property (weak, nonatomic) IBOutlet UILabel *fecha_label;

@property (weak, nonatomic) IBOutlet UIButton *cancelar_button;


@property (weak, nonatomic) IBOutlet UIButton *sms_button;


@property (weak, nonatomic) IBOutlet UIButton *llamar_button;


@property (weak, nonatomic) IBOutlet UIButton *mapa_button;

@property (weak, nonatomic) IBOutlet UIButton *mapa_button_action;
@property (weak, nonatomic) IBOutlet UIButton *calificar_button;

@end
