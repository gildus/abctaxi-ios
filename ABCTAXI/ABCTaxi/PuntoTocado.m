//
//  PinOrigen.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.

#import "PuntoTocado.h"


@implementation PuntoTocado

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate{
    self = [super init];
    if (self != nil) {
        self.coordinate = coordinate;
    }
    return self;
}
@end
