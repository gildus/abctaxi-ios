//
//  PinOrigen.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PinOrigen : NSObject <MKAnnotation>

@property (nonatomic, readonly)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic)NSString *titulo;

- (id) initWithTitle: (NSString *)newTitle location: (CLLocationCoordinate2D) location;
-(MKAnnotationView *) annotation;

@end
