//
//  AppDelegate.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "Aceptado.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Parse enableLocalDatastore];
    
    // Initialize Parse.
    [Parse setApplicationId:@"1yDYn7WdjUPqTekFWQmLmFAiPjfpeqHRkfsIdjPO"
                  clientKey:@"sfqspWJTVfIGkThdY5Y5GqjV9C0et1CzLTJtodb1"];
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    // Let the device know we want to receive push notifications
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
     (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    
  
    return YES;
   
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    if (currentInstallation.badge != 0) {
        currentInstallation.badge = 0;
        [currentInstallation saveEventually];
    }
    // ...
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    
    UIApplicationState state = [application applicationState];
    
    NSLog(@"APP STATE=%d", state);
    
    _id_not= [userInfo objectForKey:@"id_not"];
    
    _idemployee= [userInfo objectForKey:@"idemployee"];
    
    _fullname= [userInfo objectForKey:@"fullname"];
    
    _phone= [userInfo objectForKey:@"phone"];
    
    _dni= [userInfo objectForKey:@"dni"];
    
    _licenseplate= [userInfo objectForKey:@"licenseplate"];
    
    _model= [userInfo objectForKey:@"model"];
    
    _color= [userInfo objectForKey:@"color"];
    
    _latitude= [userInfo objectForKey:@"latitude"];
    
    _longitude= [userInfo objectForKey:@"longitude"];
    
    
    NSLog(@"P=%@",_model);
    
    
    if ([_id_not isEqualToString:@"1"]){
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    
    Aceptado *controller = (Aceptado*)[mainStoryboard
                                                       instantiateViewControllerWithIdentifier: @"servicio_aceptado"];
        controller.id_not =_id_not;
        controller.idemployee = _idemployee;
        controller.fullname = _fullname;
        controller.phone = _phone;
        controller.photo = _photo;
        controller.dni = _dni;
        controller.licenseplate = _licenseplate;
        controller.model = _model;
        controller.color = _color;
        controller.latitude = _latitude;
        controller.longitude = _longitude;

    self.window.rootViewController = controller;
    }
    
    }
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    UIDevice *device = [UIDevice currentDevice];
     NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    NSLog(@"DEVICE=%@",currentDeviceId);
   
    // Store the deviceToken in the current Installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
  
    
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation setObject:currentDeviceId forKey:@"userDevice"];
    
    [currentInstallation saveInBackground];
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}
@end
