//
//  MainViewController.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface MapaConductor : UIViewController <MKMapViewDelegate,  CLLocationManagerDelegate, UIAlertViewDelegate>

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)//constante para definir version iOS porque a partir de la 8.0, el procedimiento de posicionamiento GPS del usuario en el mapa es diferente

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;//boton que abre side bar menu
@property(nonatomic, retain) IBOutlet MKMapView *mapView;//crea instancia del mapa

@property (weak, nonatomic) IBOutlet UIButton *boton_origen;//crea instancia del boton origen
- (IBAction)boton_origen_action:(id)sender;//crea accion del boton origen
@property (weak, nonatomic) IBOutlet UILabel *nombre_label;
@property (weak, nonatomic) IBOutlet UILabel *apellidos_label;
@property (weak, nonatomic) IBOutlet UIButton *boton_mapa;
- (IBAction)boton_mapa_action:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *boton_destino;//crea instancia del boton destino
- (IBAction)boton_destino_action:(id)sender;//crea accion del boton destino

@property (nonatomic, assign)CLLocationCoordinate2D  puntoOrigen;
@property (nonatomic, assign)CLLocationCoordinate2D puntoDestino;

//instancias para marcar la ruta
@property (weak, nonatomic) IBOutlet UILabel *destinationLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *transportLabel;
@property (weak, nonatomic) IBOutlet UITextView *steps;

@property (strong, nonatomic) NSString *allSteps;
@property (strong, nonatomic) NSString *employee;
@property (strong, nonatomic) NSString *nombre_empleado;
@property (strong, nonatomic) NSString *apellidos_empleado;


@property (weak, nonatomic) IBOutlet UIButton *boton_solicitar_taxi;

- (IBAction)boton_solicitar_action:(id)sender;
@property (nonatomic, retain) UIAlertView *myAlertType1;
@property (nonatomic, retain) UIAlertView *myAlertType2;

//para comprobar el estado del servicio de localizacion

- (IBAction)goToSettings:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *status;
@property(nonatomic)CLLocationManager *locationManager;
@property(nonatomic)NSTimer *timer;
@end
