//
//  MainViewController.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <UIKit/UIKit.h>



@interface NuevoServicioViewController : UIViewController <UIAlertViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmented_tiempo;
@property (weak, nonatomic) IBOutlet UIButton *boton_contado;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmented_pago;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;//boton que abre side bar menu
- (IBAction)aceptar_action:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *notas_text_field;
@property (weak, nonatomic) IBOutlet UIImageView *falta_direccion_origen;
@property (weak, nonatomic) IBOutlet UIImageView *falta_vehiculo;

@property (weak, nonatomic) IBOutlet UITextField *id_vehiculo_textfield;
@property (weak, nonatomic) IBOutlet UISlider *slider_equipajes;
@property (weak, nonatomic) IBOutlet UILabel *pasajeros_label;
@property (weak, nonatomic) IBOutlet UILabel *equipajes_label;
@property (weak, nonatomic) IBOutlet UIButton *boton_tomar_taxi;
- (IBAction)boton_tomar_taxi_action:(id)sender;

@property (weak, nonatomic) IBOutlet UISlider *slider_pasajeros;
@property (weak, nonatomic) IBOutlet UIButton *aceptar_button;
@property (weak, nonatomic) IBOutlet UITextField *hora_actual_textfield;
@property (weak, nonatomic) IBOutlet UITextField *hora_actual2_textfield;
@property (weak, nonatomic) NSString *mensaje_error;
- (IBAction)cambio_pago:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *tipos_auto;
@property (weak, nonatomic) IBOutlet UILabel *hora_label;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cambio_fecha;
@property (weak, nonatomic) IBOutlet UILabel *dia_label;
@property (strong, nonatomic) IBOutlet UIDatePicker *picker;
@property (weak, nonatomic) NSString *codigo_error;
- (IBAction)cambiar_passenger_slider:(id)sender;

- (IBAction)cambiar_baggage_slider:(id)sender;

@property (weak, nonatomic)UIDatePicker *datepicker;
@property (weak, nonatomic)UIPopoverController *popOverForDatePicker;
@property (weak, nonatomic)NSString *forma_pago;


@property (weak, nonatomic)NSDate *fecha_actual;
@property (weak, nonatomic)NSDate *fecha_futura;
@property (weak, nonatomic) IBOutlet UITextField *origen_textfield;

@property (weak, nonatomic)UIPickerView *tipos_autos_picker;
@property (weak, nonatomic) IBOutlet UITextField *destino_textfield;

//PARAMETROS DEL POST PARA PEDIR TAXI
//client
@property (strong, nonatomic)NSString *parametro_client;
//origin
@property (strong, nonatomic)NSString *parametro_origin;
//origin_latitude
@property (strong, nonatomic)NSString *parametro_origin_latitude;
//origin_longitude
@property (strong, nonatomic)NSString *parametro_origin_longitude;
//date
@property (strong, nonatomic)NSString *parametro_date;
//time_service
@property (strong, nonatomic)NSString *parametro_time_service;
//type_vehicle
@property (strong, nonatomic)NSString *parametro_type_vehicle;

@property (nonatomic,retain) NSURLConnection *myConnection;
@property (nonatomic,retain) NSURLConnection *mySecondConnection;
@end

