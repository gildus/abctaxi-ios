//
//  PinOrigen.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PinAsignado : NSObject <MKAnnotation>

@property (nonatomic, readonly)CLLocationCoordinate2D coordinate;
@property (copy, nonatomic)NSString *title;

- (id) initWithTitle: (NSString *)newTitle location: (CLLocationCoordinate2D) location;
-(MKAnnotationView *) annotation;

@end
