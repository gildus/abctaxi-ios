//
//  MainViewController.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <UIKit/UIKit.h>


@interface Calificaciones : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;//boton que abre side bar menu
@property (strong, nonatomic)NSString *id_service;
@property (strong, nonatomic)NSString *rate;
@property (strong, nonatomic)NSString *comment;
@property (weak, nonatomic) IBOutlet UIButton *s1;
- (IBAction)s1_action:(id)sender;
- (IBAction)s3_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *s4;
- (IBAction)s4_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *s3;
@property (weak, nonatomic) IBOutlet UITextField *comentarios_textfield;
@property (weak, nonatomic) IBOutlet UIButton *enviar_button;


@property (weak, nonatomic) IBOutlet UIButton *s5;
- (IBAction)s5_action:(id)sender;
@property (nonatomic,retain) NSURLConnection *myConnection;
@property (weak, nonatomic) IBOutlet UIButton *s2;
- (IBAction)s2_action:(id)sender;
@property (weak, nonatomic) NSString *codigo_error;
@property (weak, nonatomic) NSString *mensaje_error;
- (IBAction)enviar_button:(id)sender;

@end
