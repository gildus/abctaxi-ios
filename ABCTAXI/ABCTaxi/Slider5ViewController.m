//
//  Slider5ViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Slider5ViewController.h"
#import "SWRevealViewController.h"
#import "MainViewController.h"

@interface Slider5ViewController ()

@end

@implementation Slider5ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //swipe a la derecha para regresar a slide 4
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    //swipe a la izquierda para slide 6
    UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler1:)];
    [gestureRecognizer1 setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer1];
    

    
}
//metodo que regresa al slide 4
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider5_4" sender:self];
}

//metodo que muestra slide 6
-(void)swipeHandler1:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider5_6" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}



@end
