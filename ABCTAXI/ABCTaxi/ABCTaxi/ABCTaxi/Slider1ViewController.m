//
//  Slider1ViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Slider1ViewController.h"
#import "SWRevealViewController.h"
#import "MainViewController.h"
#import <SystemConfiguration/SCNetworkReachability.h>

@interface Slider1ViewController () <UIAlertViewDelegate>

@end

@implementation Slider1ViewController


- (void) viewDidAppear:(BOOL)animated { //metodo que se ejecuta nada mas aparecer la pantalla
    
       NSLog(@"P1");
    //verificar si hay conexion a internet
    
    BOOL hayConexion = [self isNetworkAvailable];//se crea una funcion BOOL que llama al metodo isNetworkAvailable, que es el que verifica si hay conexion a internet
    if(hayConexion)
    {
         NSLog(@"P2");
        NSLog(@"HAY CONEXION? %hhd",hayConexion);//si hay conexion, no pasa nada y el proceso sigue su curso
    }
    if (!hayConexion) // si no hay conexion, salta una alerta para cerrar la app
    {
        UIAlertView * alert =[[UIAlertView alloc ] initWithTitle:@"No hay conexión a Internet"
                                                         message:@"Esta aplicación requiere de conexión a internet para poder continuar"
                                                        delegate:self
                                               cancelButtonTitle:@"Cerrar"
                                               otherButtonTitles: nil];
      
        [alert show];
        
    }
    
    //verificar si el usuario quiere ver el slider, en el ultimo slider, Slider5ViewController, he creado una variable del sistema si el usuario pulsa el boton NO VOLVER A MOSTRAR, le asigna el valor entero 5.
    //en este metodo se lee esa variable y si su valor es igual 5, ya no se muestran el resto de sliders
    NSLog(@"P3");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//se crea una instancia de la variable del sistema
    
    NSNumber *aNumber = [defaults objectForKey:@"verSlider"];//la variable es de tipo NSNumber
    NSInteger anInt = [aNumber intValue];//para realizar la comprobacion de la variable la convierto en int
    
    if (anInt == 5){ //si la variable =5, quiere decir que el usuario ha decidido no volver a ver la demo
        NSLog(@"Saltar slider.");
        NSLog(@"P4");
     [self performSegueWithIdentifier:@"saltar_slider" sender:self];//en lugar de pasar al siguiente slider, se activa el segue que lleva al mapa de pedido de taxi
    }
    
}

// alerta que salta si no hay conexion a internet. En iOS no existe el concepto cerrar app, asi que no es muy frecuente escribir un metodo que cierre la app. En este caso, para que el funcionamiento de la app sea similar a la version Android, si no hay conexion a internet, la alerta muestra un boton para cerrar la app

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Index =%ld",(long)buttonIndex);
    if (buttonIndex == 0)
    {
        NSLog(@"Pulsado boton cerrar");
        exit(0);//cierra la app, aunque es un proceso no recomendado por Apple. Prefieren cerrar la app pulsando el boton HOME, y queda en segundo plano.
    }
}
- (void)viewDidLoad { //metodo que se ejecuta una vez cargados todos los elementos de la pantalla
    [super viewDidLoad];
    NSLog(@"P5");
    //aqui vuelvo a comprobar si el usuario quiere volver a ver la demo o no
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSNumber *aNumber = [defaults objectForKey:@"verSlider"];//la variable es de tipo NSNumber
    NSInteger anInt = [aNumber intValue];//para realizar la comprobacion de la variable la convierto en int
    
    if (anInt == 5){ //como el valor de la variable =5, la pantalla aparece el tiempo necesario para realizar la verificación de la conexion a internet
        self.view.backgroundColor = [UIColor blackColor];
        NSLog(@"Mientras salta el slider quita la imagen");
        
    }
  
    else{
            NSLog(@"P6");
        UIScrollView *scr=[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        scr.tag = 1;
        scr.autoresizingMask=UIViewAutoresizingNone;
        [self.view addSubview:scr];
        [self setupScrollView:scr];
        UIPageControl *pgCtr = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 264, 480, 36)];
        [pgCtr setTag:8];
        pgCtr.numberOfPages=6;
        pgCtr.autoresizingMask=UIViewAutoresizingNone;
        //[self.view addSubview:pgCtr];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button addTarget:self
                   action:@selector(no_volver_a_mostrar_action:)
         forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"No volver a mostrar Demo" forState:UIControlStateNormal];
        button.frame = CGRectMake(10.0, (self.view.frame.size.height)-50, 200.0, 40.0);
        [button setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:button];
        
        UIButton *button2 = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [button2 addTarget:self
                   action:@selector(saltarSlider:)
         forControlEvents:UIControlEventTouchUpInside];
        [button2 setTitle:@"Entrar" forState:UIControlStateNormal];
        button2.frame = CGRectMake(212.0, (self.view.frame.size.height)-50, 160.0, 40.0);
        [button2 setBackgroundColor:[UIColor clearColor]];
        [self.view addSubview:button2];
      
    }
    

    
}

- (void)setupScrollView:(UIScrollView*)scrMain {
    // we have 6 images here.
    // we will add all images into a scrollView & set the appropriate size.
    
    for (int i=1; i<=6; i++) {
        // create image
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"sti%02i.png",i]];
        // create imageView
        UIImageView *imgV = [[UIImageView alloc] initWithFrame:CGRectMake((i-1)*scrMain.frame.size.width, 0, scrMain.frame.size.width, scrMain.frame.size.height)];
        // set scale to fill
        imgV.contentMode=UIViewContentModeScaleToFill;
        // set image
        [imgV setImage:image];
        // apply tag to access in future
        imgV.tag=i+1;
        // add to scrollView
        [scrMain addSubview:imgV];
        
    }
    // set the content size to 10 image width
    [scrMain setContentSize:CGSizeMake(scrMain.frame.size.width*6, scrMain.frame.size.height)];
    // enable timer after each 2 seconds for scrolling.
   // [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(scrollingTimer) userInfo:nil repeats:NO];
}

- (IBAction)saltarSlider: (id) sender {
     [self performSegueWithIdentifier:@"saltar_slider" sender:self];//una vez pulsado el boton y guardado la variable abre la pantalla pedido taxi
    
}

- (IBAction)no_volver_a_mostrar_action:(id)sender {
    
    //guarda decision de volver a mostrar o no el slider en una variable de sistema
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // to store
    [defaults setObject:[NSNumber numberWithInt:5] forKey:@"verSlider"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"saltar_slider" sender:self];//una vez pulsado el boton y guardado la variable abre la pantalla pedido taxi
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



//metodo standard para comprobar si hay conexion a internet
-(bool)isNetworkAvailable
{
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReachOnExistingConnection =     success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    if( canReachOnExistingConnection )
        NSLog(@"Network available");
    else
        NSLog(@"Network not available");
    
    return canReachOnExistingConnection;
}


@end
