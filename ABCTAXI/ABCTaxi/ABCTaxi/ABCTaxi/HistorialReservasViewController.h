//
//  HistorialReservasViewController.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/13/15.

//

#import <UIKit/UIKit.h>


@interface HistorialReservasViewController : UITableViewController 

@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;//boton que abre side bar menu
@property (nonatomic,retain) NSURLConnection *myConnection;
@property (nonatomic,retain) NSURLConnection *mySecondConnection;
@property (nonatomic, strong) NSMutableData *responseData;
@property (weak, nonatomic) NSString *codigo_error;
@property (weak, nonatomic) NSString *mensaje_error;

@end