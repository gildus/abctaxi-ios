

#import <UIKit/UIKit.h>

@interface SidebarTableViewController : UITableViewController


- (IBAction)login_button_action:(id)sender;
- (IBAction)pedir_taxi_button_action:(id)sender;
- (IBAction)salir_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *en_sesion_pidauntaxi_button;

@property (weak, nonatomic) IBOutlet UIButton *en_sesion_misreservas_button;

@property (weak, nonatomic) IBOutlet UIButton *en_sesion_cerrarsesion_button;

@property (weak, nonatomic) IBOutlet UIButton *no_sesion_pidauntaxi_button;

@property (weak, nonatomic) IBOutlet UIButton *no_sesion_iniciarsesion_button;


@property (weak, nonatomic) IBOutlet UIButton *no_sesion_salir_button;
@property (weak, nonatomic) IBOutlet UIImageView *icono_usuario;
@property (weak, nonatomic) IBOutlet UILabel *nombre_label;
@property (weak, nonatomic) IBOutlet UIView *barra_amarilla;

@property (weak, nonatomic) IBOutlet UILabel *email_label;
@property (weak, nonatomic) IBOutlet UIButton *cerrar_sesion_button;
- (IBAction)cerrar_sesion_action:(id)sender;
@end
