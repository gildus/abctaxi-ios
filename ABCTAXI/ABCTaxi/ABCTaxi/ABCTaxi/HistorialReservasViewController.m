//
//  HistorialReservasViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/13/15.

//

#import "HistorialReservasViewController.h"

#import "MainViewController.h"
#import "SWRevealViewController.h"
#import "HistorialCellTableViewCell.h"
#import "NSObject+NSDictionary.m"


@interface HistorialReservasViewController () {
    
}

@end

@implementation HistorialReservasViewController
@synthesize mensaje_error,codigo_error;
NSMutableArray *myObject;
// A dictionary object
NSDictionary *dictionary;
// Define keys
NSString *title;
NSString *thumbnail;
NSString *author;
NSMutableArray  *historialServicios;

-(void) viewWillAppear:(BOOL)animated{
    [self relanzar_request ];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    

    
    self.
    //codigo del side bar menu
     self.title = @"Taxi Corona"; //titulo de la pantalla
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    NSLog(@"POS 5");
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    NSLog(@"POS 6");
   
    [self relanzar_request];
}



   
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return historialServicios.count;
}
- (NSString *)stringValueOfDictionaryObject:(id)dictionaryObject
{
    if (dictionaryObject == [NSNull null]) {
        return @"";
    }
    else {
        return (NSString *)dictionaryObject;
    }
}

- (NSNumber *)numericValueOfDictionaryObject:(id)dictionaryObject
{
    if (dictionaryObject == [NSNull null]) {
        return nil;
    }
    else {
        return (NSNumber *)dictionaryObject;
    }
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (connection == self.myConnection){
        mensaje_error = nil;
        codigo_error = nil;
        
        NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
        if(responseString) {
            NSLog(@"dATOS RECIBIDOS=%@", responseString);
            
            
            
            NSError *jsonError;
            NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            for (id key in json) {
                NSLog(@"key: %@, value: %@", key, [json objectForKey:key]);
                //NSString *minombre = [json objectForKey:@"name"];
                // NSLog(@"Mi nombre =%@", minombre);
                mensaje_error = [json objectForKey:@"message"];
                BOOL mensaje_recibido = [json objectForKey:@"message"];
                NSLog(@"POS1");

                
                if (mensaje_recibido){
                    NSLog(@"POS2");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Servicio cancelado" message:@"El servicio ha sido cancelado" delegate:self
                                                             cancelButtonTitle:@"Continuar" otherButtonTitles:nil, nil];
                    NSLog(@"POS3");
                    [alertView show];
                    [self relanzar_request];
                }
                    if (!mensaje_recibido){
                        NSLog(@"POS2");
                        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                                  @"Error al cancelar el servicio" message:@"El servicio no ha sido cancelado" delegate:self
                                                                 cancelButtonTitle:@"Continuar" otherButtonTitles:nil, nil];
                        NSLog(@"POS3");
                        [alertView show];
                    }
                
            }
            
            
        }
        
        
    }
  
    
    
}


-(void)cancelar_button_action:(UIButton*)sender
{
    NSLog(@"CANCELAR BUTTON CLICKED=%ld",(long)sender.tag);
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/servicess/cliente/delete/"]];
    
    NSString *idclient = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:sender.tag] valueForKey:@"client_id"]];
    NSString *idservice = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:sender.tag] valueForKey:@"id_service"]];
    NSLog(@"CLIENT ID=%@   SERVICE ID=%@",idclient, idservice);
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 idclient, @"idclient",
                                 idservice, @"idservice",
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    self.myConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
        HistorialCellTableViewCell *cell = (HistorialCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    //antes de mostrar los servicios, quitamos todos los botones
    
    cell.cancelar_button.hidden= YES;
    cell.sms_button.hidden = YES;
    cell.llamar_button.hidden =YES;
    cell.mapa_button.hidden = YES;
    cell.calificar_button.hidden = YES;
    
    //gestion de los botones
    
    cell.cancelar_button.tag = indexPath.row;
    [cell.cancelar_button addTarget:self action:@selector(cancelar_button_action:) forControlEvents:UIControlEventTouchUpInside];
    
    
    NSString *state = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"state"]];
    
  //ESTADO DEL SERVICIO
    
    
    NSLog(@"*****************************************ESTADO DEL SERVICIO =%@",state);
    
    if ([state isEqualToString:@"1"]){//EN ESPERA
        cell.conductor_label.text = @"Servicio en espera";
        NSString *conductor_nombre=
        cell.state_view.backgroundColor = [UIColor colorWithRed:(251.0 / 255.0) green:(192.0 / 255.0) blue:(22.0 / 255.0) alpha: 1];
        
        
        cell.cancelar_button.hidden =NO;

        
    }
    
    else if ([state isEqualToString:@"3"]){//ASIGNADO
        NSString *nombre_conductor = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"driverName"]];
        
        NSString *apellidos_conductor = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"driverLastname"]];
        NSMutableString *conductor = [[NSMutableString alloc]initWithString:nombre_conductor];
        [conductor appendString:@"  "];
        [conductor appendString:apellidos_conductor];
        cell.conductor_label.text = conductor;
        cell.cancelar_button.hidden = NO;

        
      
        cell.state_view.backgroundColor = [UIColor colorWithRed:(128.0 / 255.0) green:(128.0 / 255.0) blue:(128.0 / 255.0) alpha: 1];
        
    }
    
   else  if ([state isEqualToString:@"4"]){//FINALIZADO
    
      
        cell.state_view.backgroundColor = [UIColor colorWithRed:(252.0 / 255.0) green:(70.0 / 255.0) blue:(63.0 / 255.0) alpha: 1];
        
    }
    
    
    //campos comunes
   
     NSString *origin = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"origin"]];
    if (origin.length ==0){
        cell.origen_label.text = @"-";
    }
    else {
        
        cell.origen_label.text = origin;
    }
    
     NSString *destiny = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"destiny"]];
    if (destiny.length ==0){
        cell.destiny_label.text = @"-";
    }
    else {
        
        cell.destiny_label.text = destiny;
    }


    
     NSString *date = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"date"]];
    
    
    NSString *dateStr=date;
    // Convertimos el string date a objeto de fecha
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
      [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"]];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *fecha = [dateFormat dateFromString:dateStr];
    NSLog(@"DATE CONVERTIDO A FECHA =%@", fecha);
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"es_ES"]];
    
    [df setDateFormat:@"EEEE dd MMMM yyyy"];
    
    
    NSString *newDatestr = [df stringFromDate:fecha];
    
    NSLog(@"date: %@", newDatestr);
    
    NSMutableString *texto_fecha = [[NSMutableString alloc] initWithString:newDatestr];
    NSString *hora_servicio = [self stringValueOfDictionaryObject:(id)[[historialServicios objectAtIndex:indexPath.row] valueForKey:@"time_service"]];
    [texto_fecha appendString:@" "];
    [texto_fecha appendString:hora_servicio];
    
    cell.fecha_label.text = texto_fecha;
    
    
    
    
    
    return cell;
}


-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) relanzar_request {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//se crea una instancia de la variable del sistema
    
    
    //   NSNumber *aNumber = [defaults objectForKey:@"sesionIniciada"];//la variable es de tipo NSNumber
    NSString *idcliente = [defaults objectForKey:@"idUsuario"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *date_actual = [NSDate date];
    NSString *fecha_hoy = [dateFormat stringFromDate:date_actual];
    
    NSLog(@"FECHA ACTUAL CONVERTIDA A STRING=%@", fecha_hoy);
    
    NSLog (@"HE ENTRADO EN HISTORIAL VC");
    myObject = [[NSMutableArray alloc] init];
    NSURL *apiURL = [NSURL URLWithString:
                     [NSString stringWithFormat:@"http://abctaxi-studio.abcdroid.com/rest/clienthistoric/?client=%@&date=%@", idcliente,fecha_hoy]];
    NSURLRequest *request = [NSURLRequest requestWithURL:apiURL];
    
    NSLog (@"HE MANDADO LA REQUEST DEL HISTORIAL");
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(data.length) {
                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   if(responseString && responseString.length) {
                                       NSLog(@"dATOS RECIBIDOS EN HISTORIAL=%@", responseString);
                                       
                                       NSLog (@"HE RECIBIDO LA REQUEST DEL HISTORIAL");
                                       NSError *jsonError;
                                       NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                            options:NSJSONReadingMutableContainers
                                                                                              error:&jsonError];
                                       NSArray *messageArray = [json objectForKey:@"objects"];
                                       
                                       historialServicios = [[NSMutableArray alloc]init];
                                       
                                       // Parse and loop through the JSON
                                       for (dictionary in messageArray) {
                                           NSString * date = [dictionary optionalObjectForKey:@"date" defaultValue:[NSNull null]];
                                           NSString * origin = [dictionary optionalObjectForKey:@"origin" defaultValue:[NSNull null]];
                                           NSString * destiny = [dictionary optionalObjectForKey:@"destiny" defaultValue:[NSNull null]];
                                           NSString * rate = [dictionary optionalObjectForKey:@"service_rate" defaultValue:[NSNull null]];
                                           NSString * state = [dictionary optionalObjectForKey:@"state" defaultValue:[NSNull null]];
                                           NSString * time_service = [dictionary optionalObjectForKey:@"time_service" defaultValue:[NSNull null]];
                                           NSString * id_service = [dictionary optionalObjectForKey:@"id" defaultValue:[NSNull null]];
                                           
                                           //datos de nivel cliente
                                           NSDictionary *level2Dict = [dictionary optionalObjectForKey:@"client" defaultValue:[NSDictionary dictionary]];
                                           NSString *client_id = [level2Dict optionalObjectForKey:@"id" defaultValue:[NSNull null]];
                                           
                                           //datos de nivel cabdriver
                                           NSDictionary *cabdriverLevelDict=[dictionary optionalObjectForKey:@"cabdriver" defaultValue:[NSDictionary dictionary]];
                                           
                                           //datos de nivel employee
                                           NSDictionary *employeeLevelDict = [cabdriverLevelDict optionalObjectForKey:@"employee" defaultValue:[NSDictionary dictionary]];
                                           
                                           //datos del employee
                                           NSString *driverName = [employeeLevelDict optionalObjectForKey:@"name" defaultValue:[NSNull null]];
                                           NSString *driverLastname = [employeeLevelDict optionalObjectForKey:@"lastname" defaultValue:[NSNull null]];
                                           NSString *driverPhone = [employeeLevelDict optionalObjectForKey:@"phone" defaultValue:[NSNull null]];
                                           NSString *driverId = [employeeLevelDict optionalObjectForKey:@"id" defaultValue:[NSNull null]];
                                           
                                           
                                           [historialServicios addObject:@{
                                                                           @"time_service": time_service,
                                                                           @"id_service": id_service,
                                                                           @"rate": rate,
                                                                           @"destiny": destiny,
                                                                           @"state": state,
                                                                           @"origin": origin,
                                                                           @"client_id":client_id,
                                                                           @"date": date,
                                                                           @"driverName":driverName,
                                                                           @"driverLastname": driverLastname,
                                                                           @"driverPhone": driverPhone,
                                                                           @"driverId": driverId
                                                                           
                                                                           }];
                                           NSLog(@"DESPUES DE ANADIR OBJETOS");
                                           
                                           NSLog(@"OBJETO ANADIDO==>TIME SERVICE = %@, ID SERVICE=%@, SERVICE RATE=%@,SERVICE DATE=%@,DESTINY=%@, STATE =%@,CLIENT ID=%@, ORIGIN=%@,DRIVER NAME=%@, DRIVER LASTNAME=%@,DRIVER PHONE=%@, DRIVER ID=%@",time_service,id_service,rate,date,destiny,state,client_id,origin,driverName,driverLastname,driverPhone,driverId);
                                           
                                           //insertamos objetos en diccionario historialServicios
                                       }
                                       NSLog(@"NUMERO DE ITEMS=%lu", (unsigned long)[historialServicios count]);
                                       [self.tableView reloadData];
                                       if ([historialServicios count]==0){
                                           UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                                                     @"No hay servicios que mostrar" message:@"No hay ninguna reserva" delegate:self
                                                                                    cancelButtonTitle:@"Continuar" otherButtonTitles:nil, nil];
                                           NSLog(@"POS3");
                                           [alertView show];
                                       }
                                       NSLog(@"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++SERVICIOS RECIBIDOS=%@", historialServicios);
                                   }
                                   
                               }
                           }];
}
@end

