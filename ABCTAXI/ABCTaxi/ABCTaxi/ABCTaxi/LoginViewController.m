
#import "LoginViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>



@interface LoginViewController () <UIAlertViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableData *responseData;
@end

@implementation LoginViewController
@synthesize mensaje_error,codigo_error;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.alerta_email.hidden = YES;
    
    self.alerta_clave.hidden = YES;
     self.title = @"Taxi Corona"; //titulo de la pantalla
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
        [self.email_textfield resignFirstResponder];

     self.responseData = [NSMutableData data];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.alerta_email.hidden = YES;
    
    self.alerta_clave.hidden = YES;
    textField.layer.cornerRadius=8.0f;
    textField.layer.masksToBounds=YES;
    textField.layer.borderColor=[[UIColor yellowColor]CGColor];
    textField.layer.borderWidth= 1.0f;
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.layer.cornerRadius=8.0f;
    textField.layer.masksToBounds=YES;
    textField.layer.borderColor=[[UIColor clearColor]CGColor];
    textField.layer.borderWidth= 1.0f;
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    if (textField == self.email_textfield) {
        [self.clave_textfield becomeFirstResponder];
    }
       return YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
  self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"response data - %@", [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding]);
    
    
    mensaje_error = nil;
    codigo_error = nil;
    
    NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    if(responseString) {
        NSLog(@"dATOS RECIBIDOS=%@", responseString);
        
        
        
        NSError *jsonError;
        NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        for (id key in json) {
            NSLog(@"key: %@, value: %@", key, [json objectForKey:key]);
            NSString *minombre = [json objectForKey:@"name"];
             NSString *miemail = [json objectForKey:@"email"];
            NSString *miId = [json objectForKey:@"id"];
            NSLog(@"Mi nombre =%@", minombre);
            mensaje_error = [json objectForKey:@"message"];
            codigo_error = [json objectForKey:@"code"];
            
            
            if (mensaje_error){
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:codigo_error message:mensaje_error delegate:self cancelButtonTitle:@"DATOS ERRONEOS" otherButtonTitles:nil, nil];
                [alertView show];
                return;
            }
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            // to store
            [defaults setObject:[NSNumber numberWithInt:1] forKey:@"sesionIniciada"];
            
            NSString *emailusuario = miemail;
            [[NSUserDefaults standardUserDefaults] setObject:emailusuario forKey:@"emailUsuario"];
            
            NSString *nombreusuario = minombre;
            [[NSUserDefaults standardUserDefaults] setObject:nombreusuario forKey:@"nombreUsuario"];
            
            NSString *idusuario = miId;
            [[NSUserDefaults standardUserDefaults] setObject:idusuario forKey:@"idUsuario"];
            
            [defaults synchronize];
            [self performSegueWithIdentifier:@"pedir_taxi_segue" sender:self];//una vez pulsado el boton y guardado la variable abre la pantalla pedido taxi
            
            
        }
        
      
        
    }
    
    
    
    
    

}

- (IBAction)login_button_action:(id)sender {
    
    
    if ([self.email_textfield.text isEqualToString:@""]){
        self.alerta_email.hidden = NO;
        return;
    }
    if ([self.clave_textfield.text isEqualToString:@""]){
        self.alerta_clave.hidden = NO;
        return;
    }

    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/user/login/"]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  self.email_textfield.text, @"email",
                                 self.clave_textfield.text, @"password",
                                 self.clave_textfield.text, @"android_id",
                        
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
  
    
}
- (void) alertStatus:(NSString *)msg :(NSString *) title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}



@end
