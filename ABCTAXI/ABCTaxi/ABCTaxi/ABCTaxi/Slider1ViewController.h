//
//  Slider1ViewController.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <UIKit/UIKit.h>

@interface Slider1ViewController : UIViewController <UIScrollViewDelegate>


- (void)setupScrollView:(UIScrollView*)scrMain ;


@end
