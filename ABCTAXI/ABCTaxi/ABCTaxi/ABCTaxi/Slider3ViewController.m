//
//  Slider3ViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Slider3ViewController.h"
#import "SWRevealViewController.h"
#import "MainViewController.h"

@interface Slider3ViewController ()

@end

@implementation Slider3ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //swipe a la derecha para regresar a slide 2
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    //swipe a la izquierda para slide 4
    UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler1:)];
    [gestureRecognizer1 setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer1];
    
    
}
//metodo que regresa al slide 2
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider3_2" sender:self];
}
//metodo que abre slide 4
-(void)swipeHandler1:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider3_4" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
