//
//  Slider4ViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Slider4ViewController.h"
#import "SWRevealViewController.h"
#import "MainViewController.h"

@interface Slider4ViewController ()

@end

@implementation Slider4ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    //swipe a la derecha para regresar a slide 3
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    //swipe a la izquierda para slide 5
    UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler1:)];
    [gestureRecognizer1 setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer1];
    
    
}
//metodo que regresa a slide 3
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider4_3" sender:self];
}
//metodo que muestra slide 5
-(void)swipeHandler1:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider4_5" sender:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}


@end
