//
//  NSObject+NSDictionary.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/18/15.
//  Copyright (c) 2015 AppCoda. All rights reserved.
//

#import "NSObject+NSDictionary.h"

@interface NSDictionary (NilNull)
- (id)optionalObjectForKey:(id)key;
- (id)optionalObjectForKey:(id)key defaultValue:(id)defaultValue;
@end

@implementation NSDictionary (NilNull)
- (id)optionalObjectForKey:(id)key {
    return [self optionalObjectForKey:key defaultValue:nil];
}
    - (id)optionalObjectForKey:(id)key defaultValue:(id)defaultValue {
        id obj = [self objectForKey:key];
        return (obj == [NSNull null] || !obj) ? defaultValue : obj;
    }
    @end