

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) NSString *photoFilename;
@property (weak, nonatomic) IBOutlet UITextField *email_textfield;
@property (weak, nonatomic) IBOutlet UITextField *clave_textfield;
@property (weak, nonatomic) IBOutlet UIButton *registrar_button;
@property (weak, nonatomic) IBOutlet UIImageView *alerta_email;

@property (weak, nonatomic) IBOutlet UIImageView *alerta_clave;

@property (weak, nonatomic) IBOutlet UIButton *iniciar_sesion_button;
- (IBAction)login_button_action:(id)sender;
@property (weak, nonatomic) NSString *mensaje_error;

@property (weak, nonatomic) NSString *codigo_error;

@end
