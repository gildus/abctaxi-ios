//
//  MainViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "NuevoServicioViewController.h"
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import "PinOrigen.h"
#import "PinDestino.h"
#import "PinDisponible.h"
#import "PuntoTocado.h"





@interface NuevoServicioViewController () <UIGestureRecognizerDelegate,UITextFieldDelegate>

@property (nonatomic, strong) NSMutableData *responseData;

@end

@implementation NuevoServicioViewController

UIGestureRecognizer *tapper;
NSArray *_pickerData;//aqui iran los tipos de vehiculos

@synthesize mensaje_error,codigo_error;







- (void)viewDidLoad {
    [super viewDidLoad];
    
    //OCULTAMOS AL PRINCIPIO ALGUNOS ELEMENTOS
   
    self.aceptar_button.hidden = YES;
    self.segmented_pago.hidden = YES;
    self.title = @"Taxi Corona"; //titulo de la pantalla
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    NSLog(@"POS 5");
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    NSLog(@"POS 6");
    //VALORES QUE SERAN LOS PARAMETROS QUE PASAR EN EL POST DE PEDIDO DE TAXI
    //1. client , lo tomamos de la variable del sistema
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//se crea una instancia de la variable del sistema

    
 //   NSNumber *aNumber = [defaults objectForKey:@"sesionIniciada"];//la variable es de tipo NSNumber
    NSString *idcliente = [defaults objectForKey:@"idUsuario"];
    NSLog(@"ID CLIENTE=&%@",idcliente);
    
    self.parametro_client = idcliente;
    
    NSLog(@"PARAMETRO DEFINITIVO client = %@", self.parametro_client);
    
    //2. origin,  lo tomamos del textfield una vez pulsamos el boton TOMAR TAXI, PENDIENTE
    
    
    //3. origin_latitude, la recibimos de mainview
    
    NSLog(@"LATITUD RECIBIDA DE MAINVIEW=%@",self.parametro_origin_latitude);
    
    //4. origin_longitude, la recibimos de mainview
    
    
    NSLog(@"LONGITUD RECIBIDA DE MAINVIEW=%@",self.parametro_origin_longitude);
    
    
    //5. date, lo tomamos de aqui mas abajo
    
    //6. time_service, lo tomamos de aqui mas abajo
    
    
    //7. type_vehicle, lo recibimos de un get request del server
    //ELEMENTO 1, FORMA DE PAGO
    //POR DEFECTO CONTADO
    
    //SERVICIO REST PARA COMPROBAR SI ESTA ASOCIADO A UNA EMPRESA
    
    
    
  
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/clientecode/company/"]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 idcliente, @"idclient",
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    self.myConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    
    //ELEMENTO 2, AHORA Y DESPUES
    
    
    
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    NSLog(@"FECHA=%@", dateString);
    
    
    self.parametro_date = dateString;
    self.dia_label.text = dateString;
    
    // get current date/time
    
    // get current date/time
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];
    NSLog(@"HORA=%@", dateString);
    
    self.parametro_time_service = dateString;
    self.hora_label.text = dateString;
    
   
    
    //ELEMENTO 3 AUTOS
    
    
    //  request para descargar los tipos de autos
    
    
    
    
    NSURL *apiURL = [NSURL URLWithString:
                     [NSString stringWithFormat:@"http://abctaxi-studio.abcdroid.com/rest/type_vehicle/"]];
    NSURLRequest *request2 = [NSURLRequest requestWithURL:apiURL]; // this is using GET, for POST examples see the other answers here on this page
    [NSURLConnection sendAsynchronousRequest:request2
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(data.length) {
                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   if(responseString && responseString.length) {
                                       NSLog(@"dATOS RECIBIDOS=%@", responseString);
                                       NSError *jsonError;
                                       NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                            options:NSJSONReadingMutableContainers
                                                                                              error:&jsonError];
                                       NSArray *messageArray = [json objectForKey:@"objects"];
                                       _pickerData = messageArray;
                                       NSLog(@"MESSAGEARRAY=%@", _pickerData);
                                       
                                       
                                       // Parse and loop through the JSON
                                       for (NSDictionary * dataDict in messageArray) {
                                           NSString * baggage = [dataDict objectForKey:@"baggage"];
                                           NSString * pasajeros = [dataDict objectForKey:@"passenger"];
                                           NSString * name = [dataDict objectForKey:@"name"];
                                           NSLog(@"EQUIPAJES=%@",baggage);
                                           
                                       }
                                       
                                   }
                               }
                           }];
    
    
    
 
    
    
       //codigo del side bar menu
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
   }





- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self showPickerWithDoneButton:textField];
    return YES;
}

- (void)showPickerWithDoneButton:(UITextField *)sender
{
    UITextField *textField = sender;
    
    // Creamos UIPickerView como una vista personalizada de un keyboard View
    UIPickerView *pickerView = [[UIPickerView alloc] init];
    [pickerView sizeToFit];
    pickerView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    pickerView.delegate = self;
    pickerView.dataSource = self;
    pickerView.showsSelectionIndicator = YES;
    
     //UIPickerView
    
    //Asignamos el pickerview al inputView de nuestro texfield
    self.tipos_auto.inputView = pickerView;
    
    // Preparamos el botón
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    keyboardDoneButtonView.barStyle = UIBarStyleDefault;
    keyboardDoneButtonView.translucent = YES;
    keyboardDoneButtonView.tintColor = nil;
    [keyboardDoneButtonView sizeToFit];
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithTitle:         NSLocalizedString(@"Aceptar", @"Button")                                                                                    style:UIBarButtonItemStyleBordered target:self                                                                 action:@selector(pickerHechoClicked:)];
    
    doneButton.tintColor = [UIColor blackColor];
    
    //Para ponerlo a la derecha del todo voy a crear un botón de tipo Fixed Space
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace                                                                                    target:nil action:nil];
    
    fixedSpace.width = keyboardDoneButtonView.frame.size.width - 150;
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:fixedSpace, doneButton, nil]];
    
    // Finalmente colocamos la keyboardDoneButtonView  en el text field...
    textField.inputAccessoryView = keyboardDoneButtonView;
}

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [_pickerData [row] objectForKey:@"name"];
}

-(void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    NSString *tipo_vehiculo = [_pickerData [row] objectForKey:@"name"] ;
    NSString *id_vehiculo = [NSString stringWithFormat:@"%@",[_pickerData [row] objectForKey:@"id"]] ;
    NSString *num_pasajeros = [NSString stringWithFormat:@"%@",[_pickerData [row] objectForKey:@"passenger"]] ;
    NSString *num_equipajes = [NSString stringWithFormat:@"%@",[_pickerData [row] objectForKey:@"baggage"]] ;
    NSLog(@"ID VEHICULO=%@",id_vehiculo);
    NSLog(@"TIPO VEHICULO=%@",tipo_vehiculo);
    NSLog(@"PASAJEROS VEHICULO=%@",num_pasajeros);
    NSLog(@"EQUIPAJES VEHICULO=%@",num_equipajes);
    
    //cambiamos texto del text field
    self.parametro_type_vehicle = id_vehiculo;
    self.tipos_auto.text = tipo_vehiculo;
    self.id_vehiculo_textfield.text = id_vehiculo;
    self.slider_pasajeros.minimumValue = 1;
    self.slider_equipajes.minimumValue = 0;
    self.slider_pasajeros.maximumValue = [num_pasajeros floatValue];
    self.slider_equipajes.maximumValue = [num_equipajes floatValue];
}
-(void) pickerHechoClicked :(id)sender{
    [self.view endEditing:YES];
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (connection == self.myConnection){
    mensaje_error = nil;
    codigo_error = nil;
    
    NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    if(responseString) {
        NSLog(@"dATOS RECIBIDOS=%@", responseString);
        
        
        
        NSError *jsonError;
        NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        for (id key in json) {
           NSLog(@"key: %@, value: %@", key, [json objectForKey:key]);
            //NSString *minombre = [json objectForKey:@"name"];
           // NSLog(@"Mi nombre =%@", minombre);
            mensaje_error = [json objectForKey:@"message"];
                      
            NSLog(@"ASOCIADO A LA EMPRESA: %@",[json objectForKey:@"id"] );
            
            NSString *asociado = [json objectForKey:@"id"];
            if (!asociado){
                self.segmented_pago.hidden = YES;
                self.boton_contado.hidden = NO;
            }
            if (asociado){
                self.segmented_pago.hidden = NO;
                self.boton_contado.hidden = YES;
                
            }
                
            
            
        }
       
        
    }
    
    
    }
    if (connection == self.mySecondConnection){
        mensaje_error = nil;
        codigo_error = nil;
        
        NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
        if(responseString) {
            NSLog(@"dATOS RECIBIDOS=%@", responseString);
            
            
            
            NSError *jsonError;
            NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            for (id key in json) {
                NSLog(@"key: %@, value: %@", key, [json objectForKey:key]);
                //NSString *minombre = [json objectForKey:@"name"];
                // NSLog(@"Mi nombre =%@", minombre);
                mensaje_error = [json objectForKey:@"message"];
                
             
               
                
            }
            
            NSLog(@"MENSAJE RECIBIDO DE SERVICIO CREADO=%@", [json objectForKey:@"message"] );
            NSString *mensaje_recibido = [json objectForKey:@"message"];
            NSLog(@"POS1");
           
            if ([mensaje_recibido isEqualToString:@"true"]){
             NSLog(@"POS2");
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                          @"Servicio solicitado" message:@"Servicio en espera" delegate:self
                                                         cancelButtonTitle:@"Continuar" otherButtonTitles:nil, nil];
                 NSLog(@"POS3");
                [alertView show];
                 NSLog(@"POS4");
                [self performSegueWithIdentifier:@"historial_segue" sender:self];
                 NSLog(@"POS5");
            }
            else if ([mensaje_recibido isEqualToString:@"false"]){
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                          @"Error en el Servicio solicitado" message:@"Por favor, repita la solicitud" delegate:self
                                                         cancelButtonTitle:@"Continuar" otherButtonTitles:nil, nil];
                
                [alertView show];
            }
        }
        
    }
    
    
    
}








- (IBAction)cambio_pago:(id)sender {
    switch (self.segmented_pago.selectedSegmentIndex)
    {
        case 0:
            self.forma_pago = @"Contado";
            
            NSLog(@"FORMA DE PAGO=%@", self.forma_pago);
            break;
        case 1:
            self.forma_pago = @"Credito";
            
            NSLog(@"FORMA DE PAGO=%@", self.forma_pago);
            break;
        default: 
            break; 
    }
}

- (IBAction)cambio_fecha:(id)sender {
    switch (self.segmented_tiempo.selectedSegmentIndex)
    {
        case 0:
        {
              NSDateFormatter *formatter;
            NSString        *dateString;
            
            formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            
            dateString = [formatter stringFromDate:[NSDate date]];
            NSLog(@"FECHA=%@", dateString);
            
            
            self.parametro_date = dateString;
            self.dia_label.text = dateString;
            
            // get current date/time
            formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"HH:mm:ss"];
            
            dateString = [formatter stringFromDate:[NSDate date]];
            NSLog(@"HORA=%@", dateString);
            
            self.parametro_time_service = dateString;
            self.hora_label.text = dateString;
            self.aceptar_button.hidden = YES;
            
            UIView *viewToRemove = [self.view viewWithTag:87];
            [viewToRemove removeFromSuperview];
            self.aceptar_button.hidden = YES;
            
                  }
            break;
        case 1:
        {
           
           
                self.aceptar_button.hidden = NO;
                self.picker = [[UIDatePicker alloc] init];
                self.picker.autoresizingMask = UIViewAutoresizingFlexibleWidth;
                self.picker.datePickerMode = UIDatePickerModeDateAndTime;
            
                [self.picker addTarget:self action:@selector(dueDateChanged:) forControlEvents:UIControlEventValueChanged];
                CGSize pickerSize = [self.picker sizeThatFits:CGSizeZero];
                self.picker.frame = CGRectMake(0.0, 250, pickerSize.width, 460);
                self.picker.backgroundColor = [UIColor whiteColor];
                [self.view addSubview:self.picker];
                self.picker.tag = 87;
            
                
                
            
            
          
        }
        default:
            break;
    }
}
-(void) dueDateChanged:(UIDatePicker *)sender {
    NSDate *today = [NSDate date];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [df stringFromDate:today];
    dateString = [NSString stringWithFormat:@"%@",[df stringFromDate:sender.date]];
    self.dia_label.text = dateString;
    self.parametro_date = dateString;
    NSLog(@"FECHA DEL SERVICIO=%@", dateString);
    
    [df setDateFormat:@"HH:mm:ss"];
    NSString *dateString2 = [df stringFromDate:today];
    dateString2 = [NSString stringWithFormat:@"%@",[df stringFromDate:sender.date]];
    self.hora_label.text = dateString2;
    self.parametro_time_service= dateString2;
    NSLog(@"HORA DEL SERVICIO=%@", dateString2);
}
- (IBAction)aceptar_action:(id)sender {
    
    //COMPROBAMOS SI LA FECHA ES MAYOR DE LA FECHA  Y HORA ACTUAL +1 H
    
   
    
    //FECHA DE PARTIDA, ES LA FECHA ACTUAL
    NSDate *ahora = [NSDate date];
    NSLog(@"FECHA ACTUAL PARA COMPARAR=%@", ahora);
    // FECHA DEL SERVICIO ES LA FECHA DEL PICKER
    NSDate *propuesta = self.picker.date;
    NSLog(@"FECHA DEL PICKER A COMPARAR:%@", propuesta);
    //CALCULAMOS LA DIFERENCIA EN MINUTOS
    NSTimeInterval distanciaEntreFechas = [propuesta timeIntervalSinceDate:ahora];
    double segundosEnUnaHora = 3600;
    NSInteger minutosDeDiferencia = (distanciaEntreFechas / segundosEnUnaHora)*60;
    NSLog(@"MINUTOS DE DIFERENCIA=%ld",(long)minutosDeDiferencia);
    //COMO SE PIDE QUE AL MENOS HAYA 1 HORA DE DIFERENCIA,PARA NO AJUSTAR TANTO,DAMOS UN MARGEN DE 58 MINUTOS
    if (minutosDeDiferencia <= 58){
        NSLog(@"NO SE PUEDE REALIZAR LA RESERVA");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"NO ES POSIBLE REALIZAR LA RESERVA." message:@"Solo se aceptan reservas con al menos 1 hora de antelación" delegate:self cancelButtonTitle:@"Volver a intentar" otherButtonTitles:nil, nil];
        [alertView show];
        
        return;
    }
    
    
    
    UIView *viewToRemove = [self.view viewWithTag:87];
    [viewToRemove removeFromSuperview];
    self.aceptar_button.hidden = YES;
}


- (IBAction)cambiar_passenger_slider:(id)sender {
    self.slider_pasajeros = (UISlider *)sender;
    int valorSlider = (int)roundf(self.slider_pasajeros.value);
    self.pasajeros_label.text = [NSString stringWithFormat:@"%d", valorSlider];
}

- (IBAction)cambiar_baggage_slider:(id)sender {
    self.slider_equipajes = (UISlider *)sender;
    int valorSlider = (int)roundf(self.slider_equipajes.value);
    self.equipajes_label.text = [NSString stringWithFormat:@"%d", valorSlider];
}

- (IBAction)boton_tomar_taxi_action:(id)sender {
    //COMPROBAMOS QUE ESTAN TODOS LOS PARAMETROS
    //PARAMETRO 1 CLIENT
    NSLog(@"client = %@", self.parametro_client);
    //PARAMETRO 2 ORIGIN
   
    if ([self.origen_textfield.text isEqualToString:@""]){
        NSLog(@"direccion origen vacia");
        self.falta_direccion_origen.hidden= NO;
        return;
    }
    self.falta_direccion_origen.hidden= YES;
    self.parametro_origin = self.origen_textfield.text;
      NSLog(@"origin = %@", self.parametro_origin);
    //PARAMETRO 3 ORIGIN_LATITUDE
    NSLog(@"origin_latitude = %@", self.parametro_origin_latitude);
    //PARAMETRO 4 ORIGIN_LATITUDE
    NSLog(@"origin_longitude = %@", self.parametro_origin_longitude);
    //PARAMETRO 5. DATE
    self.parametro_date = self.dia_label.text;
    NSLog(@"date = %@", self.parametro_date);
    //PARAMETRO 6. TIME_SERVICE
    self.parametro_time_service = self.hora_label.text;
    NSLog(@"time_service = %@", self.parametro_time_service);
    //PARAMETRO 7 TYPE_VEHICLE
    
    if (!self.parametro_type_vehicle ){
        self.falta_vehiculo.hidden =NO;
        NSLog(@"tipo de vehiculo vacio");
        return;
    }
     self.falta_vehiculo.hidden =YES;
    NSLog(@"type_vehicle = %@", self.parametro_type_vehicle);

    
    //COMPROBADOS TODOS LOS PARAMETROS SE INICIA EL POST REQUEST PARA TOMAR UN TAXI
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/clientreservation/"]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 self.parametro_client, @"client",
                                 self.parametro_origin, @"origin",
                                 self.parametro_origin_latitude, @"origin_latitude",
                                 self.parametro_origin_longitude, @"origin_longitude",
                                 self.parametro_date, @"date",
                                 self.parametro_time_service,@"time_service",
                                 self.parametro_type_vehicle,@"type_vehicle",
                            
                                 
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    self.mySecondConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    




    
    
    
    
}

@end
