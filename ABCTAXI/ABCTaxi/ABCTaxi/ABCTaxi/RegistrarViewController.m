
#import "RegistrarViewController.h"
#import "SWRevealViewController.h"



@interface RegistrarViewController () <UIAlertViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableData *responseData;
@end

@implementation RegistrarViewController

UIGestureRecognizer *tapper;


@synthesize mensaje_error,codigo_error;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"Taxi Corona"; //titulo de la pantalla
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

        [self.email_textfield resignFirstResponder];

     self.responseData = [NSMutableData data];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    self.alerta_email.hidden = YES;
    
    self.alerta_clave.hidden = YES;
    
    self.alerta_nombre.hidden = YES;
    
    self.alerta_apellidos.hidden = YES;
    
    self.alerta_tel.hidden = YES;
    
    
    tapper = [[UITapGestureRecognizer alloc]
              initWithTarget:self action:@selector(handleSingleTap:)];
    tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];

}
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    if (textField == self.email_textfield) {
        [self.clave_textfield becomeFirstResponder];
    }
    else if (textField == self.clave_textfield) {
        [self.clave_repetida_textfield becomeFirstResponder];
    }
    else if (textField == self.clave_repetida_textfield) {
        [self.nombre_textfield becomeFirstResponder];
    }
    else if (textField == self.nombre_textfield) {
        [self.apellidos_textfield becomeFirstResponder];
    }
    else if (textField == self.apellidos_textfield) {
        [self.tel_textfield becomeFirstResponder];
    }
    return YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
  self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {

    mensaje_error = nil;
    codigo_error = nil;
    
    NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    if(responseString) {
        NSLog(@"dATOS RECIBIDOS=%@", responseString);
        
        
        
        NSError *jsonError;
        NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                             options:NSJSONReadingMutableContainers
                                                               error:&jsonError];
        for (id key in json) {
            NSLog(@"key: %@, value: %@", key, [json objectForKey:key]);
            NSString *minombre = [json objectForKey:@"name"];
            NSLog(@"Mi nombre =%@", minombre);
            mensaje_error = [json objectForKey:@"message"];
            codigo_error = [json objectForKey:@"code"];
            
            
            NSString *idusuario = [json objectForKey:@"id"];;
            [[NSUserDefaults standardUserDefaults] setObject:idusuario forKey:@"idUsuario"];
            
            
            
            
            NSLog(@"ID DEL USUARIO=>%@",idusuario);
            
            
            
            }
        //si hay error
        
        if (mensaje_error){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:codigo_error message:mensaje_error delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
            
            
        } else {
        //si no hay error
            
            
            
        }
        
        }
    
    
            
    

    
}


    

- (IBAction)registrar_button_action:(id)sender {
    
    if ([self.email_textfield.text isEqualToString:@""]){
        self.alerta_email.hidden = NO;
        return;
    }
    if ([self.clave_textfield.text isEqualToString:@""]){
        self.alerta_clave.hidden = NO;
        return;
    }
    if ([self.clave_repetida_textfield.text isEqualToString:@""]){
        self.alerta_clave.hidden = NO;
        return;
    }
    if ([self.nombre_textfield.text isEqualToString:@""]){
        self.alerta_nombre.hidden = NO;
        return;
    }
    if ([self.apellidos_textfield.text isEqualToString:@""]){
        self.alerta_apellidos.hidden = NO;
        return;
    }
    if ([self.tel_textfield.text isEqualToString:@""]){
        self.alerta_tel.hidden = NO;
        return;
    }
    if (![self.clave_textfield.text isEqualToString:self.clave_repetida_textfield.text]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Las contraseñas no coinciden." message:@"Vuelva a intentarlo" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    if (![self.mySwitch isOn]){
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Debe aceptar los términos y condiciones." message:@"Vuelva a intentarlo" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if(![self.email_textfield.text isEqualToString:@""] )
    {
        
        NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        //Valid email address
        if ([emailTest evaluateWithObject:self.email_textfield.text] == YES)
        {
            
            NSLog(@"EMAIL ES VALIDO");
        }
        else
        {
            
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Email con formato incorrecto." message:@"Vuelva a intentarlo" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
             NSLog(@"EMAIL NO ES VALIDO");
            return;
           

        }
    }
    
    
    else
    {
        //any of the text field is empty
    }
    
    
    UIDevice *device = [UIDevice currentDevice];
    
    NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
    NSLog(@"ID DEL DISPOSITIVO=%@", currentDeviceId);
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/client/"]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  self.email_textfield.text, @"email",
                                 self.clave_textfield.text, @"password",
                                 self.nombre_textfield.text, @"name",
                                 self.apellidos_textfield.text, @"lastname",
                                 self.tel_textfield.text, @"phone",
                                 currentDeviceId,@"device_id",
                                 @"1",@"type_device",
                                 
                                 
                                 
                                 
                                 
                                 
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    
    //si el registro es correcto se guarda la variavle del sistema para sesion iniciada
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // to store
    [defaults setObject:[NSNumber numberWithInt:1] forKey:@"sesionIniciada"];
    
    NSString *emailusuario = self.email_textfield.text;
    [[NSUserDefaults standardUserDefaults] setObject:emailusuario forKey:@"emailUsuario"];
    
    NSString *nombreusuario = self.nombre_textfield.text;
    [[NSUserDefaults standardUserDefaults] setObject:nombreusuario forKey:@"nombreUsuario"];
    
    
    NSString *deviceidusuario = self.nombre_textfield.text;
    [[NSUserDefaults standardUserDefaults] setObject:currentDeviceId forKey:@"deviceIdUsuario"];
    
    
    [defaults synchronize];
    [self performSegueWithIdentifier:@"pedir_taxi_segue" sender:self];//una vez pulsado el boton y guardado la variable abre la pantalla pedido taxi
    
}


//verificar si la direccion email es valida
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.alerta_email.hidden = YES;
    
    self.alerta_clave.hidden = YES;
    
    self.alerta_nombre.hidden = YES;
    
    self.alerta_apellidos.hidden = YES;
    
    self.alerta_tel.hidden = YES;
    textField.layer.cornerRadius=8.0f;
    textField.layer.masksToBounds=YES;
    textField.layer.borderColor=[[UIColor yellowColor]CGColor];
    textField.layer.borderWidth= 1.0f;
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    textField.layer.cornerRadius=8.0f;
    textField.layer.masksToBounds=YES;
    textField.layer.borderColor=[[UIColor clearColor]CGColor];
    textField.layer.borderWidth= 1.0f;
    return YES;
}
- (void) alertStatus:(NSString *)msg :(NSString *) title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK final" otherButtonTitles:nil, nil];
    [alertView show];
}
- (IBAction)abrir_url:(id)sender {
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.taxicorona.com/tyc/"]];
}
@end
