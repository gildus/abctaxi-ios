

#import <UIKit/UIKit.h>

@interface RegistrarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarButton;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (strong, nonatomic) NSString *photoFilename;
@property (weak, nonatomic) IBOutlet UITextField *email_textfield;
@property (weak, nonatomic) IBOutlet UITextField *clave_textfield;
@property (weak, nonatomic) IBOutlet UITextField *clave_repetida_textfield;
@property (weak, nonatomic) IBOutlet UITextField *nombre_textfield;
@property (weak, nonatomic) IBOutlet UITextField *apellidos_textfield;
@property (weak, nonatomic) IBOutlet UITextField *tel_textfield;
@property (weak, nonatomic) IBOutlet UIButton *registrar_button;
- (IBAction)registrar_button_action:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *alerta_email;
@property (weak, nonatomic) IBOutlet UIImageView *alerta_clave;
@property (weak, nonatomic) IBOutlet UIImageView *alerta_nombre;
@property (weak, nonatomic) IBOutlet UIImageView *alerta_apellidos;
@property (weak, nonatomic) IBOutlet UIImageView *alerta_tel;
- (IBAction)abrir_url:(id)sender;

@property (weak, nonatomic) IBOutlet UISwitch *mySwitch;
@property (weak, nonatomic) NSString *mensaje_error;

@property (weak, nonatomic) NSString *codigo_error;
@end
