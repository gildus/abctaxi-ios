//
//  Slider2ViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Slider2ViewController.h"
#import "SWRevealViewController.h"
#import "MainViewController.h"

@interface Slider2ViewController ()

@end

@implementation Slider2ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //swipe a la derecha para regresar al slide 1
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];

    //swipe a la izquierda para mostrar slide 3
    UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler1:)];
    [gestureRecognizer1 setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer1];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
//metodo del swipe que regresa al slide 2
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
   [self performSegueWithIdentifier:@"slider2_1" sender:self];
}
//metodo que lleva al slide 3
-(void)swipeHandler1:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider2_3" sender:self];
}


@end
