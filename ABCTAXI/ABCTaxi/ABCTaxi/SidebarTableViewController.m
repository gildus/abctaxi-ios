

#import "SidebarTableViewController.h"
#import "SWRevealViewController.h"
#import "RegistrarViewController.h"

@interface SidebarTableViewController ()

@end

@implementation SidebarTableViewController {
    NSArray *menuItems;
    NSInteger sesion;
}
@synthesize email_label,en_sesion_cerrarsesion_button,en_sesion_misreservas_button,en_sesion_pidauntaxi_button,no_sesion_iniciarsesion_button,no_sesion_pidauntaxi_button,no_sesion_salir_button,nombre_label,icono_usuario,barra_amarilla;

-(void) viewWillAppear:(BOOL)animated{
    //comprobamos si el usuario ha iniciado sesion
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//se crea una instancia de la variable del sistema
    
    NSNumber *aNumber = [defaults objectForKey:@"sesionIniciada"];//la variable es de tipo NSNumber
    sesion = [aNumber intValue];//para realizar la comprobacion de la variable la convierto en int
    NSLog (@"SESION INICIADA will appear =%ld", (long)sesion);
    if (sesion ==1){
        
        NSLog(@"SESION INICIADA");
        no_sesion_iniciarsesion_button.hidden = YES;
        no_sesion_pidauntaxi_button.hidden = YES;
        no_sesion_salir_button.hidden = YES;
        
        
        en_sesion_cerrarsesion_button.hidden = NO;
        en_sesion_misreservas_button.hidden = NO;
        en_sesion_pidauntaxi_button.hidden = NO;
        icono_usuario.hidden = NO;
        nombre_label.hidden = NO;
        email_label.hidden = NO;
        barra_amarilla.hidden = NO;
        
        NSString *idusuario = [defaults objectForKey:@"idUsuario"];
        NSLog(@"ID DEL USUARIO EN SIDEBAR =>%@", idusuario);
        NSString *emailusuario = [defaults objectForKey:@"emailUsuario"];
        email_label.text = emailusuario;
        NSString *nombreusuario = [defaults objectForKey:@"nombreUsuario"];
        nombre_label.text = nombreusuario;
    }
    if (sesion !=1){
        NSLog(@"SESION NO INICIADA");
        en_sesion_cerrarsesion_button.hidden = YES;
        en_sesion_misreservas_button.hidden = YES;
        en_sesion_pidauntaxi_button.hidden = YES;
        icono_usuario.hidden = YES;
        nombre_label.hidden = YES;
        email_label.hidden = YES;
        barra_amarilla.hidden = YES;
        no_sesion_iniciarsesion_button.hidden = NO;
        no_sesion_pidauntaxi_button.hidden = NO;
        no_sesion_salir_button.hidden = NO;
        
        
    }
    
    
   
  
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //comprobamos si el usuario ha iniciado sesion
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//se crea una instancia de la variable del sistema
    
    NSNumber *aNumber = [defaults objectForKey:@"sesionIniciada"];//la variable es de tipo NSNumber
    sesion = [aNumber intValue];//para realizar la comprobacion de la variable la convierto en int
    NSLog (@"SESION INICIADA view did load=%ld", (long)sesion);
    
    UILabel* lbNavTitle = [[UILabel alloc] initWithFrame:CGRectMake(0,40,320,40)];
    lbNavTitle.textAlignment = UITextAlignmentLeft;
    lbNavTitle.text = NSLocalizedString(@"Taxi Corona",@"");
    lbNavTitle.textColor = [UIColor whiteColor];
    self.navigationItem.titleView = lbNavTitle;
    
 
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (IBAction)login_button_action:(id)sender {
    
    NSLog(@"PULSADO INICIAR SESION");
    [self performSegueWithIdentifier:@"login_segue" sender:self];
}

- (IBAction)pedir_taxi_button_action:(id)sender {
    
    NSLog(@"PULSADO PEDIR TAXI");
    [self performSegueWithIdentifier:@"pedir_taxi_segue" sender:self];
}
- (IBAction)salir_action:(id)sender {
    
    NSLog(@"PULSADO cerrar sesion");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // to store
    [defaults setObject:[NSNumber numberWithInt:0] forKey:@"sesionIniciada"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"login_segue" sender:self];


}




- (IBAction)cerrar_sesion_action:(id)sender {
    NSLog(@"PULSADO cerrar sesion");
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // to store
    [defaults setObject:[NSNumber numberWithInt:0] forKey:@"sesionIniciada"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"login_segue" sender:self];
}
@end
