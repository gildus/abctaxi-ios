//
//  MainViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "MainViewController.h"
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import "PinOrigen.h"
#import "PinDestino.h"
#import "PinDisponible.h"
#import "PuntoTocado.h"
#import "NuevoServicioViewController.h"
#import <Parse/Parse.h>




@interface MainViewController () <UIGestureRecognizerDelegate>

@property (nonatomic, strong) NSMutableData *responseData;

@end

@implementation MainViewController

MKPolyline *_routeOverlay;
MKRoute *_currentRoute;
NSInteger sesion;
NSDictionary  *dataDict;

@synthesize mapView;





- (void)viewDidLoad {
    [super viewDidLoad];
    
    PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
    testObject[@"foo"] = @"bar";
    [testObject saveInBackground];
    self.title = @"Taxi Corona"; //titulo de la pantalla
    
    self.mapView.delegate = self;
    
   
    self.locationManager.delegate = self;
    self.locationManager = [[CLLocationManager alloc] init];
    if(IS_OS_8_OR_LATER) {
        NSLog(@"POS 1");
       [self.locationManager requestWhenInUseAuthorization];
        NSLog(@"POS 2");
        [self.locationManager requestAlwaysAuthorization];
    }
     NSLog(@"POS 3");
    [self.locationManager startUpdatingLocation];
     NSLog(@"POS 4");
    self.mapView.showsUserLocation = YES;
      NSLog(@"POS 4");
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    NSLog(@"POS 5");
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
NSLog(@"POS 6");
 
   
    [mapView setCenterCoordinate:mapView.userLocation.location.coordinate animated:YES];
   NSLog(@"POS 7");
    
    mapView.showsUserLocation = YES; // muestra un circulo azul en la posicion actual GPS
    
    [mapView setMapType:MKMapTypeStandard]; // muestra el tipo de mapa standard
    
    [mapView setZoomEnabled:YES];//habilita funcion zoom
    
    [mapView setScrollEnabled:YES];//habilita el scroll del mapa
    
    
     MKUserTrackingBarButtonItem *buttonItem = [[MKUserTrackingBarButtonItem alloc] initWithMapView:self.mapView];
    self.navigationItem.rightBarButtonItem = buttonItem;
    
    
    
   
    //crea la region visible del mapa dependiendo de la posicion GPS del usuario, muestra una region de 800x800 m)
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 800, 800);
    
    
    [self.mapView setRegion:region animated:YES];
    
    //para detectar si el usuario mueve el mapa
    UIPanGestureRecognizer* panRec = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didDragMap:)];
    [panRec setDelegate:self];
    [self.mapView addGestureRecognizer:panRec];
    
    
    //para detectar el gesto tap y centrar mapa en el punto tocado
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getCoordinatesFromTap:)];
    [self.mapView addGestureRecognizer:tapRecognizer];
    
    
    //codigo del side bar menu
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    //  request para descargar la posicion de los vehiculos disponibles
    
    NSString *latitud = self.deviceLat;
    
    NSString *longitud = self.deviceLon;
    
    NSLog (@"latitud actual =%@",latitud);
    NSLog (@"longitud actual =%@",longitud);
    
    
    
    NSURL *apiURL = [NSURL URLWithString:
                     [NSString stringWithFormat:@"http://abctaxi-studio.abcdroid.com/rest/clientaxiradio/?current_latitude=%@&current_longitude=%@", latitud,longitud]];
    NSURLRequest *request = [NSURLRequest requestWithURL:apiURL]; // this is using GET, for POST examples see the other answers here on this page
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(data.length) {
                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   if(responseString && responseString.length) {
                                       NSLog(@"dATOS RECIBIDOS=%@", responseString);
                                       NSError *jsonError;
                                       NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
                                       NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                                            options:NSJSONReadingMutableContainers
                                                                                              error:&jsonError];
                                       NSArray *messageArray = [json objectForKey:@"objects"];
                                       
                                       // Parse and loop through the JSON
                                       for (dataDict in messageArray) {
                                           NSString * latstring = [dataDict objectForKey:@"current_latitude"];
                                           NSString * lonstring = [dataDict objectForKey:@"current_longitude"];
                                           
                                           NSDictionary *level2Dict = [dataDict objectForKey:@"employee"];
                                            id someObject = [level2Dict objectForKey:@"name"];
                                           NSLog(@"NOMBRE===%@",someObject);
                                           NSString * nombre = someObject;
                                           double latdouble = [latstring doubleValue];
                                           double londouble = [lonstring doubleValue];
                                           
                                           CLLocationCoordinate2D vehiculo = [mapView centerCoordinate];
                                           vehiculo.latitude = latdouble;
                                           vehiculo.longitude = londouble;
                                           
                                           
                                           PinDisponible *vehiculoDisponible = [[PinDisponible alloc] initWithTitle:@"Vehiculo disponible" location:vehiculo];
                                           vehiculoDisponible.title = nombre;
                                           
                                           [self.mapView addAnnotation:vehiculoDisponible];
                                           [mapView selectAnnotation:vehiculoDisponible animated:NO];
                                          
                                       }
                                       
                                       
                                       
                                   }
                               }
                           }];
    

    
   }



- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)aUserLocation {
    MKCoordinateRegion region;
    
    CLLocationCoordinate2D location;
    location.latitude = aUserLocation.coordinate.latitude;
    location.longitude = aUserLocation.coordinate.longitude;
    region.center = location;
    region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.coordinate, 800, 800);
    
    
    [self.mapView setRegion:region animated:YES];
    [aMapView setRegion:region animated:YES];
}

//metodo para permitir el uso de varios gestos sobre la pantalla
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

//metodo para reconocer cuando el usuario deja de mover el mapa
- (void)didDragMap:(UIGestureRecognizer*)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded){
        NSLog(@"drag ended");
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}








- (NSString *)deviceLocation {
    
    //metodo que devuelve las coordenadas del usuario
    return [NSString stringWithFormat:@"latitude: %f longitude: %f", self.locationManager.location.coordinate.latitude, self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceLat {
    //metodo que devuelve la latitud del usuario
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.latitude];
}
- (NSString *)deviceLon {
    //metodo que devuelve la longitud del usuario
    return [NSString stringWithFormat:@"%f", self.locationManager.location.coordinate.longitude];
}
- (NSString *)deviceAlt {
    //metodo que devuelve la altitud del usuario
    return [NSString stringWithFormat:@"%f", self.locationManager.location.altitude];
}

//accion de pulsar el boton origen
- (IBAction)boton_origen_action:(id)sender {
    
    //verificamos si ya hay alguna anotacion de Origen, y si la hay la borra
    for (id <MKAnnotation> annotation in self.mapView.annotations)
    {
        if ([annotation isKindOfClass:[PinOrigen class]])
        {
            [self.mapView removeAnnotation:annotation];
        }
        
    }
    //ponemos en el punto deseado un marcador de tipo PinOrigen
    
    CLLocationCoordinate2D centre = [mapView centerCoordinate];
   
    centre.latitude += self.mapView.region.span.latitudeDelta * 0.045;//corrige la latitud para colocar el punto justo a los pies del pin del hombrecillo
    
    PinOrigen *puntoOrigen = [[PinOrigen alloc] initWithTitle:@"Origen" location:centre];
    [self.mapView addAnnotation:puntoOrigen];
    
    //asigna el punto marcado como coordenada origen
    
    self.puntoOrigen = centre;
    NSLog(@"LATITUD PUNTO ORIGEN = %f",self.puntoOrigen.latitude);
    
    NSLog(@"LONGITUD ORIGEN = %f",self.puntoOrigen.longitude);
    
    
}

//accion de pulsar el boton destino
- (IBAction)boton_destino_action:(id)sender {
    
    //verificamos si ya hay alguna anotacion de Destino, y si la hay la borra
    for (id <MKAnnotation> annotation in self.mapView.annotations)
    {
        if ([annotation isKindOfClass:[PinDestino class]])
        {
            [self.mapView removeAnnotation:annotation];
        }
        
    }
    //ponemos en el punto deseado un marcador de tipo PinOrigen
    
    CLLocationCoordinate2D centre = [mapView centerCoordinate];
    
    centre.latitude += self.mapView.region.span.latitudeDelta * 0.045;//corrige la latitud para colocar el punto justo a los pies del pin del hombrecillo
    
    PinDestino *puntoDestino = [[PinDestino alloc] initWithTitle:@"Destino" location:centre];
    [self.mapView addAnnotation:puntoDestino];
    
    //asigna el punto marcado como coordenada destino
    
    self.puntoDestino = centre;
    NSLog(@"LATITUD PUNTO DESTINO = %f",self.puntoDestino.latitude);
    
    NSLog(@"LONGITUD destino = %f",self.puntoDestino.longitude);
    
    
    //DIBUJAR RUTA ENTRE LOS DOS PUNTOS , NO IMPLEMENTADO EN PERU TODAVIA, PERO LA APP NO SE CUELGA
    
    // Make a directions request
    MKDirectionsRequest *directionsRequest = [MKDirectionsRequest new];
    
    // Start at our current location
    
    CLLocationCoordinate2D sourceCoords = CLLocationCoordinate2DMake(self.puntoOrigen.latitude, self.puntoOrigen.longitude);
    

    MKPlacemark *sourcePlacemark = [[MKPlacemark alloc] initWithCoordinate:sourceCoords addressDictionary:nil];
    MKMapItem *source = [[MKMapItem alloc] initWithPlacemark:sourcePlacemark];
    
    [directionsRequest setSource:source];
    // Make the destination
    CLLocationCoordinate2D destinationCoords = CLLocationCoordinate2DMake(self.puntoDestino.latitude, self.puntoDestino.longitude);
        MKPlacemark *destinationPlacemark = [[MKPlacemark alloc] initWithCoordinate:destinationCoords addressDictionary:nil];
    MKMapItem *destination = [[MKMapItem alloc] initWithPlacemark:destinationPlacemark];
    [directionsRequest setDestination:destination];
    
    MKDirections *directions = [[MKDirections alloc] initWithRequest:directionsRequest];
    [directions calculateDirectionsWithCompletionHandler:^(MKDirectionsResponse *response, NSError *error) {
        // We're done
       
        
        // Now handle the result
        if (error) {
            NSLog(@"There was an error getting your directions");
            return;
        }
        
        // So there wasn't an error - let's plot those routes
       
        _currentRoute = [response.routes firstObject];
        NSLog(@"CURRENT RUTA=%@",_currentRoute);
        [self plotRouteOnMap:_currentRoute];
    }];

    
    
}

- (void)plotRouteOnMap:(MKRoute *)route
{
    if(_routeOverlay) {
        [self.mapView removeOverlay:_routeOverlay];
    }
    
    // Update the ivar
    _routeOverlay = route.polyline;
    
    // Add it to the map
    [self.mapView addOverlay:_routeOverlay];
    
}
- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    MKPolylineRenderer *renderer = [[MKPolylineRenderer alloc] initWithPolyline:overlay];
    renderer.strokeColor = [UIColor redColor];
    renderer.lineWidth = 4.0;
    return  renderer;
}




//metodo que gestiona las clases custom de las anotaciones
- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[PinOrigen class]])
    {
    PinOrigen *origen = (PinOrigen *)annotation;
    MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:@"PinOrigen"];
    
    if (annotationView == nil)
        annotationView = origen.annotation;
    
    else
        annotationView.annotation = annotation;
    
    return annotationView;
    
}
    else if ([annotation isKindOfClass:[PinDestino class]])
    {
        PinDestino *destino = (PinDestino *)annotation;
        MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:@"PinDestino"];
        
        if (annotationView == nil)
            annotationView = destino.annotation;
        
        else
            annotationView.annotation = annotation;
        
        return annotationView;
        
    }
    else if ([annotation isKindOfClass:[PinDisponible class]])
    {
        
        PinDisponible *destino = (PinDisponible *)annotation;
        MKAnnotationView *annotationView = [self.mapView dequeueReusableAnnotationViewWithIdentifier:@"PinDisponible"];
        
        if (annotationView == nil)
            annotationView = destino.annotation;
        
        else
            annotationView.annotation = annotation;
        
        return annotationView;
        
    }
else
    return nil;

    
}




- (void)getCoordinatesFromTap:(UITapGestureRecognizer *)recognizer{
    
    //Obtenemos el punto de toque y lo convertimos a una coordenada de mapa
    
    CGPoint point = [recognizer locationInView:self.mapView];
    CLLocationCoordinate2D tapCoordinate = [self.mapView convertPoint:point toCoordinateFromView:self.view];
    
    //Inicializamos una CLLocation con las coordenadas obtenidas y actualizamos el pin del mapa
    
    CLLocation *tapLocation = [[CLLocation alloc] initWithLatitude:tapCoordinate.latitude longitude:tapCoordinate.longitude];
    
    //Cargamos un pin con las coordenadas obtenidas e inicializamos los textos del callout
    
    PuntoTocado *annotation = [[PuntoTocado alloc] initWithCoordinate:tapLocation.coordinate];
    [annotation setTitle:@"Tu posición"];
    [annotation setSubtitle:[NSString stringWithFormat:@"%f,%f",tapLocation.coordinate.latitude,tapLocation.coordinate.longitude]];
    
    //Eliminamos el pin existente y lo añadimos con la misma posición
    
    
    //Centramos el mapa en la localización proporcionada
    
   
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(tapCoordinate, 800, 800);
    
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];

    [self.mapView setRegion:region animated:YES];
    
}
- (IBAction)boton_solicitar_action:(id)sender {
    
    //comprobamos si el usuario ha iniciado sesion
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];//se crea una instancia de la variable del sistema
    
    NSNumber *aNumber = [defaults objectForKey:@"sesionIniciada"];//la variable es de tipo NSNumber
    sesion = [aNumber intValue];//para realizar la comprobacion de la variable la convierto en int
    NSLog (@"SESION INICIADA will appear =%ld", (long)sesion);
    if (sesion ==1){
        
        //comprobamos si ha elegido al menos el punto de origen
        NSLog(@"LATITUD PUNTO ORIGEN=%f", self.puntoOrigen.latitude);
        NSLog(@"LONGITUD PUNTO ORIGEN=%f", self.puntoOrigen.longitude);
        
        if(!self.puntoOrigen.latitude){
        NSLog(@"NO HA SELECCIONADO PUNTO ORIGEN");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Debe seleccionar un punto de recojo" message:@"" delegate:self cancelButtonTitle:@"Aceptar" otherButtonTitles:nil,nil];
            [alert setTag:1];
            [alert show];
        }
        else {
        NSLog(@"SESION INICIADA, PUEDE SOLICITAR TAXI");
         [self performSegueWithIdentifier:@"nuevo_servicio_segue" sender:self];
        }
    }
    if (sesion != 1){
       
        
        NSLog(@"SESION NO INICIADA, NO PUEDE SOLICITAR TAXI");
    // alerta con dos opciones
        
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                  @"No has iniciado sesión" message:@"Para poder continuar escoje una de las siguientes opciones" delegate:self
                                                 cancelButtonTitle:@"Regístrame" otherButtonTitles:@"Iniciar sesión", nil];
        [alertView setTag:2];
        [alertView show];
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"nuevo_servicio_segue"])
    {
        NSLog(@"estoy en segue pasando a nuevo servicio");
        // Get reference to the destination view controller
        NuevoServicioViewController *vc = [segue destinationViewController];
        //pasamos la latitud del PO
        //la convertimos a String
        NSString *latitud  = [NSString stringWithFormat:@"%.20f", self.puntoOrigen.latitude];
        vc.parametro_origin_latitude = latitud;
        //lo comprobamos
        NSLog(@"LATITUD DEL PO PASADA=%@",latitud);
        //pasamos la lONGITUD del PO
        //la convertimos a String
        NSString *longitud  = [NSString stringWithFormat:@"%.20f", self.puntoOrigen.longitude];
        vc.parametro_origin_longitude = longitud;
        //lo comprobamos
        NSLog(@"LONGITUD DEL PO PASADA=%@",longitud);
  
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
         
        {
            
               NSLog(@"boton pulsado TAG 1");
            return;
        }
        else if (buttonIndex == 1)
        {
            
        }
    }
    else if (alertView.tag == 2)
    {
        if (buttonIndex == 0)
        {
            NSLog(@"boton pulsado TAG 2");
            [self performSegueWithIdentifier:@"iniciar_registro_segue" sender:self];
            
        }
        else if (buttonIndex == 1)
        {
             [self performSegueWithIdentifier:@"iniciar_sesion_segue" sender:self];
            
        }
    }
}


@end
