//
//  PinOrigen.h
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface PuntoTocado : NSObject <MKAnnotation>


@property (nonatomic) CLLocationCoordinate2D coordinate;

@property (copy,nonatomic) NSString *title;
@property (copy,nonatomic) NSString *subtitle;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
