
#import "PhotoViewController.h"
#import "SWRevealViewController.h"



@interface PhotoViewController () <UIAlertViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableData *responseData;
@end

@implementation PhotoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
        [self.email_textfield resignFirstResponder];

     self.responseData = [NSMutableData data];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
    if (textField == self.email_textfield) {
        [self.clave_textfield becomeFirstResponder];
    }
    else if (textField == self.clave_textfield) {
        [self.clave_repetida_textfield becomeFirstResponder];
    }
    else if (textField == self.clave_repetida_textfield) {
        [self.nombre_textfield becomeFirstResponder];
    }
    else if (textField == self.nombre_textfield) {
        [self.apellidos_textfield becomeFirstResponder];
    }
    else if (textField == self.apellidos_textfield) {
        [self.tel_textfield becomeFirstResponder];
    }
    return YES;
}


- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
  self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"response data - %@", [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding]);
}

- (IBAction)registrar_button_action:(id)sender {
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/client/"]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                  self.email_textfield.text, @"email",
                                 self.clave_textfield.text, @"password",
                                 self.nombre_textfield.text, @"name",
                                 self.apellidos_textfield.text, @"lastname",
                                 self.tel_textfield.text, @"phone",
                                 
                                 
                                 
                                 
                                 
                                 
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
  
    
}
- (void) alertStatus:(NSString *)msg :(NSString *) title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:msg delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}
@end
