//
//  Slider5ViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Slider6ViewController.h"
#import "SWRevealViewController.h"
#import "MainViewController.h"

@interface Slider6ViewController ()

@end

@implementation Slider6ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //swipe a la derecha para regresar a slide 5
    UISwipeGestureRecognizer *gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler:)];
    [gestureRecognizer setDirection:(UISwipeGestureRecognizerDirectionRight)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
    //swipe a la izquierda para comenzar app
    UISwipeGestureRecognizer *gestureRecognizer1 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeHandler1:)];
    [gestureRecognizer1 setDirection:(UISwipeGestureRecognizerDirectionLeft)];
    [self.view addGestureRecognizer:gestureRecognizer1];
    

    
}
//metodo que regresa al slide 5
-(void)swipeHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"slider6_5" sender:self];
}

//metodo que abre la pantalla mapa, pedido de taxi
-(void)swipeHandler1:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received.");
    [self performSegueWithIdentifier:@"iniciar" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}

- (IBAction)no_volver_a_mostrar_action:(id)sender {
    
    //guarda decision de volver a mostrar o no el slider en una variable de sistema
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // to store
    [defaults setObject:[NSNumber numberWithInt:5] forKey:@"verSlider"];
    [defaults synchronize];
    [self performSegueWithIdentifier:@"iniciar" sender:self];//una vez pulsado el boton y guardado la variable abre la pantalla pedido taxi
    
}

@end
