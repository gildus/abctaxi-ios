//
//  MainViewController.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.


#import "Calificaciones.h"
#import "SWRevealViewController.h"

#import "NuevoServicioViewController.h"




@interface Calificaciones ()  <UIAlertViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) NSMutableData *responseData;

@end

@implementation Calificaciones







- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.title = @"Taxi Corona"; //titulo de la pantalla
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
    NSLog(@"POS 5");
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
NSLog(@"POS 6");
 
   [self.comentarios_textfield resignFirstResponder];
    self.rate =@"1";
    self.comment = @"";
    
    

    
    
    //codigo del side bar menu
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.sidebarButton setTarget: self.revealViewController];
        [self.sidebarButton setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    
    
    
    
   }

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    
 
    return YES;
}
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    self.responseData = [NSMutableData data];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    NSLog(@"didFailWithError");
    NSLog([NSString stringWithFormat:@"Connection failed: %@", [error description]]);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (connection == self.myConnection){
        self.mensaje_error = nil;
        self.codigo_error = nil;
        
        NSString *responseString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
        if(responseString) {
            NSLog(@"dATOS RECIBIDOS=%@", responseString);
            
            
            
            NSError *jsonError;
            NSData *objectData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            for (id key in json) {
                NSLog(@"key: %@, value: %@", key, [json objectForKey:key]);
                //NSString *minombre = [json objectForKey:@"name"];
                // NSLog(@"Mi nombre =%@", minombre);
                BOOL mensaje_recibido = [json objectForKey:@"message"];
                NSLog(@"POS1");
                
                
                if (mensaje_recibido){
                    NSLog(@"POS2");
                    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:
                                              @"Sus comentarios han sido registrados" message:@"" delegate:self
                                                             cancelButtonTitle:@"Continuar" otherButtonTitles:nil, nil];
                    NSLog(@"POS3");
                    [alertView show];
              
                }
                
                
            }
            
            
        }
        
        
    }
    
    
    
}






- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}






- (IBAction)s1_action:(id)sender {
    [self.s1 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s2 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    [self.s3 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    [self.s4 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    [self.s5 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    self.rate = @"1";

}
- (IBAction)s2_action:(id)sender {
    [self.s1 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s2 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s3 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    [self.s4 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    [self.s5 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    self.rate = @"2";
}
- (IBAction)s3_action:(id)sender {
    [self.s1 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s2 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s3 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s4 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    [self.s5 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    self.rate = @"3";
}
- (IBAction)s4_action:(id)sender {
    [self.s1 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s2 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s3 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s4 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s5 setImage:[UIImage imageNamed:@"star_gris.png"] forState:UIControlStateNormal];
    self.rate = @"4";
}
- (IBAction)s5_action:(id)sender {
    [self.s1 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s2 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s3 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s4 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    [self.s5 setImage:[UIImage imageNamed:@"star_azul.png"] forState:UIControlStateNormal];
    self.rate = @"5";
}
- (IBAction)enviar_button:(id)sender {
    
    self.comment = self.comentarios_textfield.text;
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://abctaxi-studio.abcdroid.com/rest/clienteupdate/choices/"]];
    
    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 self.id_service, @"idservice",
                                 self.rate, @"rate",
                                 
                                 self.comment, @"comment",
                                 nil];
    NSError *error;
    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postData];
    self.myConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    

}
@end
