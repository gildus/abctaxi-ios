//
//  PinOrigen.m
//  ABCTaxi
//
//  Created by MODESTO VASCO FORNAS on 3/6/15.

#import "PinAsignado.h"


@implementation PinAsignado


//creamos una clase custom de una anotacion en el mapa 
- (id) initWithTitle: (NSString *)newTitle location: (CLLocationCoordinate2D) location
{
    self =  [super init];
    
    if (self){
        _title = newTitle;
        _coordinate = location;
        
    }
    return self;
}
-(MKAnnotationView *) annotation
{
    MKAnnotationView *annotationView = [[MKAnnotationView alloc] initWithAnnotation:self reuseIdentifier:@"PinDisponible"];
    annotationView.enabled = YES;
    annotationView.canShowCallout = YES;
    annotationView.image = [UIImage imageNamed:@"ico_car.png"];
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:nil];
    return annotationView;
}

@end
